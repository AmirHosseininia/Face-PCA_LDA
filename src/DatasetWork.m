%turn images into matrix.
%address of image file.
clear;
clc;
str='C:\Users\Amir Hosseininia\Desktop\Face PCA-LDA\orl faces';
%training set.
train=[];
noc = 40;
nec = 10;
ntrain = 8;
%train from 1 to 8 for training.
for i=1:40
    for j=1:8
        img=imread([str,'\s',num2str(i),'\',num2str(j),'.pgm']);
        %get gray image from colored ones.
        if size(img,3) > 1
            img = rgb2gray(img);
        end
        [irow, icol] = size(img);
        img2 = im2double(img); 
        temp = reshape(img2',irow * icol , 1);
        train = [train temp];
    end
end
%test set from 9 to 10 for testing.
test=[];
for i=1:40
    for j=9:10
        img=imread([str,'\s',num2str(i),'\',num2str(j),'.pgm']); 
        if size(img,3)>1
            img=rgb2gray(img);
        end 
        [irow, icol] = size(img);
        img2 = im2double(img);
        temp = reshape(img2',irow * icol , 1);
        test = [test temp];% 'T' grows after each turn
    end
end

%train labeling
label_train = zeros(40*8,40);
for i = 1:40
    for j = 1:8
        label_train(8*(i-1)+j,i)=1;
    end
end
%test labeling
label_test = zeros(40*1, 40);
for i=1:40
    for j = 1: 2
        label_test(2*(i-1)+j,i) = 1;
    end 
end
clear i j irow icol img str temp img2
save('Data\TrainTest.mat', 'label_test', 'label_train', 'noc', 'nec','ntrain','test','train');