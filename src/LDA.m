%ovl dataset contains 40 people faces.
%number of classes.
noc = 40;
load('C:\Users\Amir Hosseininia\Desktop\Face PCA-LDA\Data\TrainTest.mat');
%we use 8 images of each class for training.
%number of training images.
ntrain = 8;
[X, Y]= size(pimage);
m1 = zeros(X, noc);
%computing the average
avg = mean(pimage,2);
for i = 1: noc
    m1(:,i) = mean(pimage(:,(i-1)*ntrain+ 1:(i-1)*ntrain+ntrain),2);
end
m2 = zeros(X,X);
for i = 1: noc
    for j = 1: ntrain
        temp = pimage(:, (i-1)*ntrain+j);
        m2 = m2+(temp-m1(i))*(temp-m1(i))';
    end
end
m3=zeros(X,X);
for i = 1:noc
    m3 = m3 + ntrain*((m1(i)-avg)*(m1(i)-avg)');
end
% find eigen values and eigen vectors
[evec,eval]=eig(m3,m2);