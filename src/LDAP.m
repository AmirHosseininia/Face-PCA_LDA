%number of eigens
load('C:\Users\Amir Hosseininia\Desktop\Face PCA-LDA\Data\TrainTest.mat');
noe = size(evec, 2);
LDAeigenfaces = evec(:,noe-40:noe);
trainLDA  = LDAeigenfaces' * pimage;
testLDA = LDAeigenfaces' * ptestimage;

LDAm1 = [];
for i = 1 : noc
    temp = mean(trainLDA(:, (i-1)*ntrain+1:(i-1)*ntrain+ntrain),2);
    LDAm1 = [LDAm1 temp];
end
%calculating distance
LDAdis = zeros(80,40);
for i = 1 : 80
    for j = 1:40
        temp1 = LDAm1(:,j);
        temp2 = ( norm( testLDA(:,i) - temp1 ) )^2;
        LDAdis(i,j) = temp2;
    end 
end

result = [];
for i = 1:80
        [~, index] = min(LDAdis(i,:));
        temp = label_test(i,index);
        result = [result temp];
end

%calculating accuracy for comparing effect of number of training images.
accuracy = sum(result) /80;