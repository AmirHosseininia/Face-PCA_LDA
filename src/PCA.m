function accuracy = recognitionPCA( test,label_test,m, cim, eigenfaces)
%UNTITLED2 Summary of this function goes here
%   This function compares two faces by projecting the images into facespace and 
%   measuring the Euclidean distance between them.
%Detailed explanation goes here
%   look eigenfaces
eigenfaces = eigenfaces(:,279:319);
pimage = [];
ntrain = size(cim,2);
for i = 1 : ntrain
    %projection of centered images into facespace
    temp2 = eigenfaces'*cim(:,i); 
    pimage = [pimage temp2]; 
end
num = numel(test(1,:));
%centered test image
diff = double(test)-m*ones(1,num); 
%test image feature vector
ptestimage = eigenfaces'*diff; 
%calculating euclidean distances 
AvgProjectImage = [];
for i = 1:40
    temp1 = pimage(:, (8*i-7):(8*i));
    temp2 = mean(temp1,2);
    AvgProjectImage=[AvgProjectImage temp2];
end
Euc_dist = zeros(40,40);
for i = 1 : 8
    for j = 1:40
        temp1 = AvgProjectImage(:,j);
        temp2 = ( norm( ptestimage(:,i) - temp1 ) )^2;
        Euc_dist(i,j) = temp2;
    end 
end
result = [];
for i = 1:40
        [~, index] = min(Euc_dist(i,:));
        temp2 = label_test(i,index);
        result = [result temp2];
end
accuracy = sum(result) /80;
save('Data\TrainTest.mat', 'pimage', 'ptestimage', 'AvgProjectImage', 'Euc_dist','result','accuracy');
end

