function [m,cim,eigenfaces] = eigenfacesfunc(ivm)
%UNTITLED Summary of this function goes here.
%   This function gets 2D matrix and returns 3outputs.
%Detailed explanation goes here.
%   arg: ivm --> image vector matrix since all k images in tarining database
%   have same size and the legth of col vectors is i*j --> ijxk.
%   outputs: mean --> mean of training data base.i*jx1.
%            cim --> centered image vectors matrix.i*jxk.
%            eigenfaces --> eigen vectors of the covariance matrix of
%            database.i*jxk-1.
%Caculating the mean image.
m = mean(ivm,2);
ntrain = size(ivm,2);
%Calculating the deviation of each image from mean image.
cim = [];
for i=1:ntrain
    %difference image for each image then merge them.
    temp = double(ivm(:, i)) - m;
    cim = [cim temp];
end
%reduction of demention ***
%surogate of covariance matrix dev*dev'.
soc = cim'*cim;
%diagonal elements of Y are the eigenvalues for dev*dev' and dev'*dev.
[X, Y] = eig(soc);
%sort and eliminat which have less than threshold eigenvalues --> nonzero
%eigenvectors are less than k-1.
eigenvec = [];
for i=1 : size(X,2)
    if(Y(i,i)>1)
        eigenvec = [eigenvec X(:,i)];
    end
end
eigenfaces = cim*eigenvec;